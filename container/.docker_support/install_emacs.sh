#!/bin/sh
# ----------------------------------------------------------------------
#  INSTALL_EMACS: Install Spacemacs
# ----------------------------------------------------------------------

set -e

# Install
apt-get update
apt-get install software-properties-common
apt-add-repository ppa:kelleyk/emacs
apt-get update
apt-get install libgmime-3.0-dev libxapian-dev emacs26 isync libgmime-3.0-0

wget --quiet https://github.com/djcb/mu/releases/download/1.2/mu-1.2.0.tar.xz
tar xf mu-1.2.0.tar.xz
cd mu-1.2.0
./configure && make && make install
ls -d /usr/share/emacs/*
cp -r mu4e/*.el* /usr/share/emacs/26*/lisp
cd ..

# Cleanup
apt-get purge software-properties-common
rm -rf /tmp/* /var/lib/apt/lists/* /root/.cache/*

# Get Spacemacs
# git clone --branch develop https://github.com/syl20bnr/spacemacs $HOME/.emacs.d
# git clone https://gitlab.com/howardabrams/spacemacs.d.git $HOME/.spacemacs.d

# mkdir $HOME/.fonts
# cd $HOME/.fonts
# wget --quiet https://github.com/i-tu/Hasklig/releases/download/1.1/Hasklig-1.1.zip

# unzip Hasklig-1.1.zip
# rm Hasklig-1.1.zip

emacs --batch --eval '(message "Initialization complete.")'

echo =====================
echo   Installed Emacs
echo =====================
echo

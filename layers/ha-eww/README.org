#+TITLE: ha-eww layer

* Table of Contents                                       :TOC_4_gh:noexport:
- [[#description][Description]]
- [[#install][Install]]
- [[#no-spacemacs][No Spacemacs]]

* Description

  For what it is, WWW is actually quite helpful.  A reader-view for articles,
  blog entries and documentation of my favorite language.  It has some hiccups
  in attempting to render certain sites, so if I use that site enough, I am
  creating a series of /cleaner/ functions.

  Why yes, this concept is /extremely fragile/, as any remote site change could
  break my cleaning scripts, but hey ... let's see how far this takes me.

* Install

  To use this configuration layer, add it to your =~/.spacemacs=. You will need to
  add =ha-eww= to the existing =dotspacemacs-configuration-layers= list in this
  file.

  *Note:* Nothing else is required, as it should /just makes things better/.

* No Spacemacs

  While this is a /layer/ in Spacemacs, you can take this for your own nefarious
  purposes, by downloading (and probably renaming) the [[file:funcs.el][funcs.el]] file, and then
  adding the following to your init:

  #+BEGIN_SRC elisp
    (add-hook 'eww-after-render-hook 'ha/eww-rerenderer)
  #+END_SRC

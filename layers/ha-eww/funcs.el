;;; FUNCS --- Cleaners and other functions for EWW
;;
;; Author: Howard X. Abrams <howard.abrams@workday.com>
;; Copyright © 2018, Howard X. Abrams, all rights reserved.
;;
;;; Code:

(defun ha/eww-wiki (text)
  "Function used to search wikipedia for the given text.
Taken from https://writequit.org/org/settings.html#sec-1-61"
  (interactive (list (read-string "Wiki for: ")))
  (eww (format "https://en.m.wikipedia.org/wiki/Special:Search?search=%s"
               (url-encode-url text))))

(defvar ha/eww-clean-reddit-comment-header
  (rx "level "
      (one-or-more digit)
      (zero-or-more anything)
      line-end

      (group (one-or-more anything))
      line-end

      (one-or-more digit)
      " points"
      (one-or-more anything)
      line-end)
  "Regular expression for matching Reddit comments")

(defun ha/eww-clean-reddit ()
  "Remove a lot of the cruft in a Reddit page rendering."
  (interactive)
  (read-only-mode -1)

  ;; 2 comments
  ;; 79% Upvoted
  ;; What are your thoughts? Log in or Sign uplog insign up
  ;; Sort by

  ;; level 1
  ;; vale_fallacia
  ;; 1 point · 12 hours ago

  (flush-lines (rx line-start
                   (zero-or-more whitespace)
                   "Submit"))
  (while (re-search-forward ha/eww-clean-reddit-comment-header nil t)
    (replace-match (concat "** " (match-string 1))))
  (read-only-mode 1))

(defvar ha/eww-clean-github-readme-ref
  (rx line-start
      " "
      (one-or-more anything)
      " README.md "
      line-end)
  "Regular expression for the README file reference that has an image in front")

(defvar ha/eww-clean-github-start-of-content
  (rx
      (one-or-more digit)
      " lines ("
      (one-or-more digit)
      " sloc) "
      (one-or-more digit)
      " ")
  "Regular expression for matching Github title")

(defvar ha/eww-clean-github-ending
  (rx "• © "
      (one-or-more digit)
      " GitHub, Inc.")
  "Github's copyright line is a good indication of the end of the content.")

(defun ha/eww-clean-github ()
  "Just to actual start of the information."
  (interactive)
  (when (re-search-forward ha/eww-clean-github-ending nil t)
    (read-only-mode -1)
    (previous-line 2)
    (delete-region (point) (point-max))
    (goto-char (point-min)))

  (when (or (re-search-forward ha/eww-clean-github-start-of-content nil t)
          (re-search-forward ha/eww-clean-github-readme-ref nil t))
    (next-line 2)
    (recenter-top-bottom 0)))

(defun ha/eww-clean-stackoverflow ()
  "Jump to the start of the good stuff on Stack Overflow"
  (interactive)
  (read-only-mode -1)

  (mapcar (lambda (regex) (flush-lines regex))
          '("^up vote "
            "^answered "
            "^asked [A-Z]" "^edited [A-Z]"
            "^add a comment "
            "^share|"
            "^active oldest"))

  (goto-char 0)
  (re-search-forward "Ask Question" nil t)
  (backward-paragraph 2)
  (next-line)
  (recenter-top-bottom 0)

  (flush-lines "^Ask Question")
  (read-only-mode 1))

(defun ha/eww-rerenderer ()
  "Calls a function based on a URL to displayed."
  (let* ((url  (url-generic-parse-url (eww-current-url)))
         (host (url-host url))
         (path (car (url-path-and-query url)))
         (bits (split-string host "\\."))
         (site (first (last bits 2))))
    (cond
     ((equal site "google")        (eww-readable))
     ((equal site "reddit")        (ha/eww-clean-reddit))
     ((equal site "github")        (ha/eww-clean-github))
     ((equal site "stackoverflow") (ha/eww-clean-stackoverflow)))))

(defun ha/eww-copy-feed-url ()
  "Take the EWW's current URL and pass it to the `feed-discovery' function."
  (interactive)
  (feed-discovery-copy-feed-url (eww-current-url)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; funcs.el ends here

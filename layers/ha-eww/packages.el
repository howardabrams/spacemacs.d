;;; PACKAGES --- ha-eww layer packages file for Spacemacs.
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;;
;;; Commentary:
;;
;; Using EWW more efficiently in Spacemacs
;;
;;; Code:

(defconst ha-eww-packages '(eww feed-discovery))

(defun ha-eww/init-eww ()
  "Emacs Web Wowzer configuration location."
  (use-package eww
    :init
    (setq browse-url-browser-function 'eww-browse-url
          shr-use-colors nil
          shr-bullet "• "
          shr-folding-mode t
          eww-search-prefix "https://duckduckgo.com/html?q="
          url-privacy-level '(email agent cookies lastloc))

    :bind (:map eww-mode-map
                (("." . scroll-up-command)))

    :config
    ;; Tweak the display of certain sites. I got back and forth on whether
    ;; maintaining this collection of functions is a good idea or not...
    ;;   (add-hook 'eww-after-render-hook 'ha/eww-rerenderer)

    (spacemacs/set-leader-keys
      "o b" 'eww
      "o B" 'ha/eww-wiki)

    ;; Many of these are repeats of the existing non-comma keysquences, or those
    ;; that might have been overshadowed by the normal state:
    (ha/set-leader-keys-for-major-mode 'eww-mode
      ","  '("scroll down"    . scroll-up-command)    ;; What is up and down seems backwards to me
      "."  '("scroll up"      . scroll-down-command)
      "q"  '("quit"           . bury-buffer)
      "G"  '("eww"            . eww)
      "g"  '("reload"         . eww-reload)
      "o"  '("links"          . ace-link-eww)
      "w"  '("copy-page-url"  . eww-copy-page-url)
      "d"  '("download"       . eww-download)
      "l"  '("back-url"       . eww-back-url)
      "r"  '("forward-url"    . eww-forward-url)
      "R"  '("reader-view"    . eww-readable)
      "n"  '("next-url"       . eww-next-url)
      "p"  '("previous-url"   . eww-previous-url)
      "H"  '("list-history"   . eww-list-histories)
      "b"  '("add-bookmark"   . eww-add-bookmark)
      "B"  '("list-bookmarks" . eww-list-bookmarks))))

(defun ha-eww/init-feed-discovery ()
  "Integration with a potential way to visit an interesting web
site, and pick up the RSS feed value for my `elfeed' work."
  (use-package feed-discovery
    :ensure t
    :config
    (ha/set-leader-keys-for-major-mode 'eww-mode
         "f" '("copy-feed-url" . ha/eww-copy-feed-url))))

;;; packages.el ends here

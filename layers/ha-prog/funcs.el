;;; FUNCS --- Functions for various programming languages
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created:  3 January 2020
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;    To get this working, first install `pyenv'.
;;
;;    Next configure `~/.envrc' to contain:
;;
;;         use_python() {
;;           local python_root=$(pyenv root)/versions/$1
;;           load_prefix "$python_root"
;;           if [[ -x "$python_root/bin/python" ]]; then
;;             layout python "$python_root/bin/python"
;;           else
;;             echo "Error: $python_root/bin/python can't be executed."
;;             exit
;;           fi
;;         }
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;;       PYTHON
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

(defun ha/python-project-initialize (version)
  "Initialize the current Projectile project for Python with LSP.
Does this with four distinct steps:

  1. Installs a particular version of Python
  2. Configures the .envrc file to use that version of Python
  3. Installs Python dependencies that Emacs/LSP needs
  4. Installs the project dependencies in the `requirements.txt' file"
  (interactive
   (list
    (completing-read "Python Version: " (shell-command-to-list "pyenv install --list"))))
  (let ((default-directory (projectile-project-root)))
    (piper-script-sh "pyenv install --skip-existing ${version}")

    (when (piper-script-file-add-or-update ".envrc"
                                           (format "use python %s" version)
                                           "use python \\(.+\\)")
      (piper-script-sh "direnv allow"))

    (ha/python-project-install-dependencies)))

(defun ha/python-project-install-dependencies ()
  "Install all a python packages via `pip' needed by both a
project (if it has a `requirements.txt' file), and by Emacs (with
the LSP project)."
  (interactive)
  (let ((default-directory (projectile-project-root))
        (pip-packages '("python-language-server[all]"
                        "ptvsd>=4.2" "epc" "importmagic"
                        "flake8" "autoflake" "nose" "yapf")))
    (dolist (package pip-packages)
      (message "Installing %s ..." package)
      (piper-script-sh "pip install ${package}"))

    (when-let* ((command (format "find '%s' -name requirements.txt" (projectile-project-root)))
                (gemfile (first (shell-command-to-list command))))
      (message "Installing project packages from %s" gemfile)
      (piper-script-sh "pip install -r ${requirements}"))))


;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;;       RUBY
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

(defun ha--ruby-versions ()
  "Returns a list of stable ruby version from `ruby-install' output."

  ;; Using the threading macro to call two different shell commands,
  ;; where the first outputs something almost like YAML, so we'll use
  ;; Ruby to parse it as YAML, and then we can split it. Yeah, I need
  ;; to write an Emacs Lisp YAML parser...
  (->> "ruby-install"
     (shell-command-to-string)
     (format "ruby -ryaml -e 'l=YAML.load(\"%s\")' -e 'puts l.values()[0][\"ruby\"]'")
     (shell-command-to-string)
     (s-trim-right)
     (s-split " ")))

(defun ha/ruby-project-initialize (version)
  "Initialize the current Projectile project for Ruby with LSP.
Does this with four distinct steps:

  1. Installs a particular version, using `ruby-install'
  2. Configures the .envrc file to use that version of Ruby
  3. Installs Ruby dependencies that Emacs/LSP needs
  4. Installs the project dependencies in the `requirements.txt' file"
  (interactive
   (list
    (completing-read "Ruby Version: " (ha--ruby-versions))))
  (let ((default-directory (projectile-project-root)))
    (piper-script
     ($ "ruby-install ruby ${version}")

     (echo "${version}@global")
     (write-into ".ruby-version"))

    (piper-script-sh "ruby-install ruby ${version}")

    (piper-script-file-create ".ruby-version" "${version}@global")

    (when (piper-script-file-add-or-update ".envrc" "use ruby ${version}"
                                           "use ruby \\(.+\\)")
      (piper-script-sh "direnv allow"))

    (ha/ruby-project-install-dependencies)))

(defun ha/ruby-project-install-dependencies ()
  "Install all a ruby packages via `gem' needed by both a
project (if it has a `Gemfile' file), and by Emacs (with
the LSP project)."
  (interactive)
  (let ((default-directory (projectile-project-root))
        (gem-packages '("solargraph" "bundler" "pry" "pry-doc" "ruby_parser"
                        "rubocop" "seeing_is_believing" "rspec")))
    (dolist (package gem-packages)
      (message "Installing %s ..." package)
      (piper-script-sh "gem install ${package}"))

    (when-let* ((command (format "find '%s' -name Gemfile" (projectile-project-root)))
                (gemfile (first (shell-command-to-list command)))
                (parent (f-parent gemfile))
                (default-directory parent))
      (message "Installing project packages from %s" gemfile)
      (piper-script-sh "bundle install"))))


;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;;       ALL LANGUAGES
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

(defun ha/compile-choice ()
  (if compile-command-list
      (cdr (assoc
            (completing-read "Command: " compile-command-list) compile-command-list))
    (read-string "Command: " compile-command)))

(defun ha/compile (command)
  "Runs the `compile' command but with choices. Set this in `dir-locals.el', like:

   (nil . ((compile-command . \"make test\")

                                   ;; Given a list of `cons' structures,  where
                                   ;; the `car' is a name for the command, and
                                   ;; the `cdr' is a compile string to execute.

           (compile-command-list . ((\"Unit tests\" . \"make test\")

                                    ;; If cdr is a list instead of a string, we evaluate it:
                                    (\"Build or Publish\" .
                                     (if (f-exists? \"foobar.rpm\") \"make rpm\" \"make publish\"))

                                    ;; List of strings are formatted:
                                    (\"Create node\" .
                                         (\"./support/create-node.sh -i %s\" (read-string \"Image ID: \")))

                                    ;; List of strings prefixed with :do are joined:
                                    (\"Build and push RPM\" .
                                       (:do \"rm -rf *.rpm\"
                                            \"make rpm\"
                                            \"make publish\"))
                                      ;; ...
                                  ))))"
  (interactive (list (ha/compile-choice)))
  (let ((default-directory (projectile-project-root))
        (final-command (if (listp command)
                           (ha/compile-eval command)
                         command)))
    (if (stringp final-command)
        (compile final-command)
      (message "The '%s' command, is not a string, but a %s" (princ final-command) (type-of final-command)))))

(defun ha/compile-eval (obj)
  "If the `ha/compile-choice' encounters a non-string (a list),
we attempt to evaluate the form returning a string."
  (cond
   ((stringp (first obj))   (apply 'format obj))
   ((equal (first obj) :do) (s-join " && " (cdr obj)))
   (t                       (format "%s" (eval obj)))))

(ert-deftest test-ha/compile-eval ()
  (should (equal "foobar xyz 4"         (ha/compile-eval '("foobar %s %d" "xyz" 4))))
  (should (equal "do this && then this" (ha/compile-eval '(:do "do this" "then this"))))
  (should (equal "4"                    (ha/compile-eval '(1+ 3)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; funcs.el ends here

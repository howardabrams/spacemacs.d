;;; FUNCS --- Special org-mode functions
;;
;; Author: Howard Abrams <howard.abrams@gmail.com>
;; Copyright © 2018, Howard Abrams, all rights reserved.
;; Created: 19 January 2018
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;; Personal enhancements (requiring new functions) to org-mode.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defun org-insert-name ()
  "Insert a #+NAME: using the Snippet Template."
  (interactive)
  (yas-use-snippet-full-line "#+NAME: ${0}"))

(defun ha/insert-two-spaces (N)
  "Inserts two spaces at the end of sentences."
  (interactive "p")
  (when (and (looking-back "[!?.] " 2)
             (not (looking-back "[0-9]\. ")))
    (insert " ")))

(advice-add 'org-self-insert-command :after #'ha/insert-two-spaces)

;; From [[http://kitchingroup.cheme.cmu.edu/blog/2017/04/09/A-better-return-in-org-mode/][this discussion]], I got the code to replace ~M-RET~ in lists with
;; just ~RET~, so that Org acts more like other word processors.

(defun ha/org-return (&optional ignore)
  "Add new list item, heading or table row with RET.
A double return on an empty element deletes it. Use a prefix arg
to get regular RET. "
  (interactive "P")
  (if ignore
      (org-return)
    (cond
     ;; Open links like usual
     ((eq 'link (car (org-element-context)))
      (org-return))
     ;; lists end with two blank lines, so we need to make sure we are also not
     ;; at the beginning of a line to avoid a loop where a new entry gets
     ;; created with only one blank line.
     ((and (org-in-item-p) (not (bolp)))
      (if (org-element-property :contents-begin (org-element-context))
          (org-insert-heading)
        (beginning-of-line)
        (setf (buffer-substring
               (line-beginning-position) (line-end-position)) "")
        (org-return)))
     ((org-at-heading-p)
      (if (not (string= "" (org-element-property :title (org-element-context))))
          (progn (org-end-of-meta-data)
                 (org-insert-heading))
        (beginning-of-line)
        (setf (buffer-substring
               (line-beginning-position) (line-end-position)) "")))
     ((org-at-table-p)
      (if (-any?
           (lambda (x) (not (string= "" x)))
           (nth
            (- (org-table-current-dline) 1)
            (org-table-to-lisp)))
          (org-return)
        ;; empty row
        (beginning-of-line)
        (setf (buffer-substring
               (line-beginning-position) (line-end-position)) "")
        (org-return)))
     (t
      (org-return)))))

(defun org-line-element-context ()
  "Return the symbol of the current block element, e.g. paragraph or list-item."
  (let ((context (org-element-context)))
    (while (member (car context) '(verbatim code bold italic underline))
      (setq context (org-element-property :parent context)))
    context))

(defun org-really-in-item-p ()
  "Similar to `org-in-item-p', however, this works around an
issue where the point could actually be in some =code= words, but
still be on an item element."
  (save-excursion
    (let ((location (org-element-property :contents-begin
                                          (org-line-element-context))))
      (when location
        (goto-char location))
      (org-in-item-p))))

(defun ha/org-return ()
  "If at the end of a line, do something special based on the
information about the line by calling `ha/org-special-return',
otherwise, just call `org-return' as usual."
  (interactive)
  (if (eolp)
      (ha/org-special-return)
    (org-return)))

(defun ha/org-special-return (&optional ignore)
  "Add new list item, heading or table row with RET.
A double return on an empty element deletes it.
Use a prefix arg to get regular RET. "
  (interactive "P")
  (if ignore
      (org-return)
    (cond
     ;; Open links like usual
     ((eq 'link (car (org-element-context)))
      (org-return))

     ;; lists end with two blank lines, so we need to make sure
     ;; we are also not at the beginning of a line to avoid a
     ;; loop where a new entry gets created with only one blank
     ;; line.
     ((and (org-really-in-item-p) (not (bolp)))
      (if (org-element-property :contents-begin (org-line-element-context))
          (progn
            (end-of-line)
            (org-insert-item))
        (delete-region (line-beginning-position) (line-end-position))
        ))

     ((org-at-heading-p)
      (if (string= "" (org-element-property :title (org-element-context)))
          (delete-region (line-beginning-position) (line-end-position))
        (org-insert-heading-after-current)))

     ((org-at-table-p)
      (if (-any?
           (lambda (x) (not (string= "" x)))
           (nth
            (- (org-table-current-dline) 1)
            (org-table-to-lisp)))
          (org-return)
        ;; empty row
        (beginning-of-line)
        (setf (buffer-substring
               (line-beginning-position) (line-end-position)) "")
        (org-return)))

     (t
      (org-return)))))

(defun ha/org-for-word-processing ()
  "These projects set up word processing style in org files."
  (interactive)
  (define-key org-mode-map (kbd "RET")  #'ha/org-return)

  (visual-line-mode 1)
  ;; (spacemacs/toggle-visual-line-navigation-on)
  (adaptive-wrap-prefix-mode 1))

;; ----------------------------------------------------------------------
;;   Org Agenda
;; ----------------------------------------------------------------------

(defun ha/todays-agenda ()
  "Display an agenda for today, including tasks and scheduled entries."
  (interactive)
  (org-ql-view "Overview: Today"))

;; Need to compare my ideas with what Spacemacs offers with ,
;; If so, add the following:
;; (with-eval-after-load "org-agenda" (ha/evil-org-agenda))

(defun ha/evil-org-agenda ()
  "Evilify the org-agenda mode similar to the evil-collection."
  (evil-set-initial-state 'org-agenda-mode 'normal)
  (evil-define-key 'normal org-agenda-mode-map
      (kbd "<RET>") 'org-agenda-switch-to
      (kbd "\t") 'org-agenda-goto

      "q" 'org-agenda-quit
      "r" 'org-agenda-redo
      "S" 'org-save-all-org-buffers
      "gj" 'org-agenda-goto-date
      "gJ" 'org-agenda-clock-goto
      "gm" 'org-agenda-bulk-mark
      "go" 'org-agenda-open-link
      "s" 'org-agenda-schedule
      "+" 'org-agenda-priority-up
      "," 'org-agenda-priority
      "-" 'org-agenda-priority-down
      "y" 'org-agenda-todo-yesterday
      "n" 'org-agenda-add-note
      "t" 'org-agenda-todo
      ":" 'org-agenda-set-tags
      ";" 'org-timer-set-timer
      "I" 'helm-org-task-file-headings
      "i" 'org-agenda-clock-in-avy
      "O" 'org-agenda-clock-out-avy
      "u" 'org-agenda-bulk-unmark
      "x" 'org-agenda-exit
      "j"  'org-agenda-next-line
      "k"  'org-agenda-previous-line
      "vt" 'org-agenda-toggle-time-grid
      "va" 'org-agenda-archives-mode
      "vw" 'org-agenda-week-view
      "vl" 'org-agenda-log-mode
      "vd" 'org-agenda-day-view
      "vc" 'org-agenda-show-clocking-issues
      "g/" 'org-agenda-filter-by-tag
      "gh" 'org-agenda-holiday
      "gv" 'org-agenda-view-mode-dispatch
      "f" 'org-agenda-later
      "b" 'org-agenda-earlier
      "c" 'helm-org-capture-templates
      "e" 'org-agenda-set-effort
      "{" 'org-agenda-manipulate-query-add-re
      "}" 'org-agenda-manipulate-query-subtract-re
      "A" 'org-agenda-toggle-archive-tag
      "." 'org-agenda-goto-today
      "0" 'evil-digit-argument-or-evil-beginning-of-line
      "<" 'org-agenda-filter-by-category
      ">" 'org-agenda-date-prompt
      "F" 'org-agenda-follow-mode
      "D" 'org-agenda-deadline
      "H" 'org-agenda-holidays
      "J" 'org-agenda-next-date-line
      "K" 'org-agenda-previous-date-line
      "L" 'org-agenda-recenter
      "P" 'org-agenda-show-priority
      "R" 'org-agenda-clockreport-mode
      "Z" 'org-agenda-sunrise-sunset
      "T" 'org-agenda-show-tags
      "X" 'org-agenda-clock-cancel
      "[" 'org-agenda-manipulate-query-add
      "g\\" 'org-agenda-filter-by-tag-refine
      "]" 'org-agenda-manipulate-query-subtract))

;; ----------------------------------------------------------------------
;;   Functions that help configure my journal
;; ----------------------------------------------------------------------

(defun get-journal-file-today ()
  "Return filename for today's journal entry."
  (let ((daily-name (format-time-string "%Y%m%d")))
    (expand-file-name (concat org-journal-dir daily-name))))

(defun todays-journal-entry ()
  "Return the full pathname to the day's journal entry file."
  (list (get-journal-file-today)))

(defun journal-file-today ()
  "Create and load a journal file based on today's date."
  (interactive)
  (find-file (get-journal-file-today)))

(defun get-journal-file-yesterday ()
  "Return filename for yesterday's journal entry."
  (let* ((yesterday (time-subtract (current-time) (days-to-time 1)))
         (daily-name (format-time-string "%Y%m%d" yesterday)))
    (expand-file-name (concat org-journal-dir daily-name))))

(defun journal-file-yesterday ()
  "Creates and load a file based on yesterday's date."
  (interactive)
  (find-file (get-journal-file-yesterday)))

;; Nice to /automatically/ insert a specific header if the journal entry
;; file is empty using [[https://www.gnu.org/software/emacs/manual/html_node/autotype/Autoinserting.html][auto-insert]].
;;
;; When I create a new journal entry, I want a snappy title and a
;; checklist of daily tasks.  The template should insert a date that
;; matches the file's name, not necessarily the current date.
;;
;; Also the inserted daily information and check-lists should only
;; happen if I am creating today's journal, not catching up with the
;; past... oh, and we might have special dailies to be inserted based
;; on the day of the week. Guess I /could/ use YAS snippets, but then the
;; code amount of code would over-shadow the text, so we'll make a
;; function.

;; To use this, make the following files:
;; - =journal-dailies.org= to contain the /real/ dailies
;; - =journal-dailies-end.org= to contain any follow-up notes
;; - =journal-mon.org= for additional text to be inserted on Monday journals
;; - And a =journal-XYZ.org= for each additional weekday
(defun journal-file-insert ()
  "Insert's the journal heading based on the file's name."
  (interactive)
  (let* ((year  (string-to-number (substring (buffer-name) 0 4)))
         (month (string-to-number (substring (buffer-name) 4 6)))
         (day   (string-to-number (substring (buffer-name) 6 8)))
         (datim (encode-time 0 0 0 day month year)))

    (insert (format-time-string org-journal-date-format datim))
    (insert "\n\n  $0\n") ; Start with a blank separating line

    ;; Note: The `insert-file-contents' leaves the cursor at
    ;; the beginning, so the easiest approach is to insert
    ;; these files in reverse order:

    ;; If the journal entry I'm creating matches today's date:
    (when (equal (file-name-base (buffer-file-name))
                 (format-time-string "%Y%m%d"))
      (insert-file-contents "journal-dailies-end.org")

      ;; Insert dailies that only happen once a week:
      (let ((weekday-template (downcase
                               (format-time-string "journal-%a.org"))))
        (when (file-exists-p weekday-template)
          (insert-file-contents weekday-template)))
      (insert-file-contents "journal-dailies.org")
      (insert "$0")

      (let ((contents (buffer-string)))
        (delete-region (point-min) (point-max))
        (yas-expand-snippet contents (point-min) (point-max))))))

(define-auto-insert "/[0-9]\\{8\\}$" [journal-file-insert])

;; I really would really like to read what I did last year
;; "at this time", and by that, I mean, 365 days ago, plus or
;; minus a few to get to the same day of the week.

(defun journal-last-year-file ()
  "Returns the string corresponding to the journal entry that
    happened 'last year' at this same time (meaning on the same day
    of the week)."
  (let* ((last-year-seconds (- (float-time) (* 365 24 60 60)))
         (last-year (seconds-to-time last-year-seconds))
         (last-year-dow (nth 6 (decode-time last-year)))
         (this-year-dow (nth 6 (decode-time)))
         (difference (if (> this-year-dow last-year-dow)
                         (- this-year-dow last-year-dow)
                       (- last-year-dow this-year-dow)))
         (target-date-seconds (+ last-year-seconds (* difference 24 60 60)))
         (target-date (seconds-to-time target-date-seconds)))
    (format-time-string "%Y%m%d" target-date)))

(defun journal-last-year ()
  "Loads last year's journal entry, which is not necessary the
    same day of the month, but will be the same day of the week."
  (interactive)
  (let ((journal-file (concat org-journal-dir (journal-last-year-file))))
    (find-file journal-file)))

;; ------------------------------------------------------------
;;   Functions that help with capturing
;; ------------------------------------------------------------

(require 'which-func)

(defun ha/org-capture-code-snippet (f)
  "Given a file, F, this captures the currently selected text
within an Org SRC block with a language based on the current mode
and a backlink to the function and the file."
  (with-current-buffer (find-buffer-visiting f)
    (let ((org-src-mode (replace-regexp-in-string "-mode" "" (format "%s" major-mode)))
          (func-name (which-function)))
      (ha/org-capture-fileref-snippet f "SRC" org-src-mode func-name))))

(defun ha/org-capture-clip-snippet (f)
  "Given a file, F, this captures the currently selected text
within an Org EXAMPLE block and a backlink to the file."
  (with-current-buffer (find-buffer-visiting f)
    (ha/org-capture-fileref-snippet f "EXAMPLE" "" nil)))

(defun ha/org-capture-fileref-snippet (f type headers func-name)
  (let* ((code-snippet
          (buffer-substring-no-properties (mark) (- (point) 1)))
         (file-name   (buffer-file-name))
         (file-base   (file-name-nondirectory file-name))
         (line-number (line-number-at-pos (region-beginning)))
         (initial-txt (if (null func-name)
                          (format "From [[file:%s::%s][%s]]:"
                                  file-name line-number file-base)
                        (format "From ~%s~ (in [[file:%s::%s][%s]]):"
                                func-name file-name line-number
                                file-base))))
    (format "
   %s

   #+BEGIN_%s %s
%s
   #+END_%s" initial-txt type headers code-snippet type)))

(defun ha/code-to-clock (&optional start end)
  "Send the currently selected code to the currently clocked-in org-mode task."
  (interactive)
  (org-capture nil "F"))

(defun ha/code-comment-to-clock (&optional start end)
  "Send the currently selected code (with comments) to the
currently clocked-in org-mode task."
  (interactive)
  (org-capture nil "f"))


;; ----------------------------------------------------------------------
;;  Functions to help convert content from the operating system's
;;  clipboard into org-mode-compatible text.
;; ----------------------------------------------------------------------

(defun shell-command-with-exit-code (program &rest args)
  "Run PROGRAM with ARGS and return the exit code and output in a list."
  (with-temp-buffer
    (list (apply 'call-process program nil (current-buffer) nil args)
          (buffer-string))))

(defun ha/get-clipboard ()
  "Returns a list where the first entry is the content type,
either :html or :text, and the second is the clipboard contents."
  (if (eq system-type 'darwin)
      (ha/get-mac-clipboard)
    (ha/get-linux-clipboard)))

(defun ha/get-linux-clipboard ()
  "Return the clipbaard for a Unix-based system. See `ha/get-clipboard'."
  (destructuring-bind (exit-code contents)
      (shell-command-with-exit-code "xclip" "-o" "-t" "text/html")
    (if (= 0 exit-code)
        (list :html contents)
      (list :text (shell-command-to-string "xclip -o")))))

(defun ha/get-mac-clipboard ()
  "Returns a list where the first entry is the content type,
either :html or :text, and the second is the clipboard contents."
  (destructuring-bind (exit-code contents)
      (shell-command-with-exit-code "osascript" "-e" "the clipboard as \"HTML\"")
    (if (= 0 exit-code)
        (list :html (ha/convert-applescript-to-html contents))
      (list :text (shell-command-to-string "osascript -e 'the clipboard'")))))

(defun ha/convert-applescript-to-html (packed-contents)
  "Applescript's clipboard returns the contents in a packed array.
Convert and return this encoding into a UTF-8 string."
  (cl-flet ((hex-pack-bytes (tuple) (string-to-number (apply 'string tuple) 16)))
    (let* ((data (-> packed-contents
                     (substring 10 -2) ; strips off the =«data RTF= and =»\= bits
                     (string-to-list)))
           (byte-seq (->> data
                          (-partition 2)  ; group each two hex characters into tuple
                          (mapcar #'hex-pack-bytes))))

      (decode-coding-string
       (mapconcat #'byte-to-string byte-seq "") 'utf-8))))

(defun ha/markdown-to-slack-buffer ()
  "Converts a Markdown-formatted buffer into Slack's odd version.
Including unfilling all the paragraphs (as line breaks don't
really mean anything in Slack), and wrapping the indented code in
triple quotes."
  (let ((initial-space (rx line-start (repeat 4 space)))
        (lite-emphasis (rx (group (not (any "*")))
                           "*"
                           (group
                            (minimal-match
                             (one-or-more
                              (not (any "*")))))
                           "*"
                           (group (not (any "*")))))
        (bold-emphasis (rx "**"
                           (group
                            (minimal-match
                             (one-or-more
                              (not (any "*")))))
                           "**"))
        ;; "\\[\\(.*?\\)\\](\\(.*?\\))"
        (md-link (rx "["
                     (group (minimal-match (one-or-more printing)))
                     "]"
                     "("
                     (group (minimal-match (one-or-more printing)))
                     ")"
                     )))

    ;; Walk through the buffer paragraph-by-paragraph unfilling each and
    ;; surrounding indented blocks with triple back-ticks:
    (goto-char (point-min))
    (unfill-paragraph)
    (while (progn
             (forward-paragraph)
             (eq 0 (forward-line)))
      (if (looking-at-p initial-space)
          (ha/markdown-to-slack-buffer-block)
        (unfill-paragraph)))

    ;; Slack uses slight different emphasis:
    (goto-char (point-min))
    (while (re-search-forward lite-emphasis nil t)
      (replace-match "\\1_\\2_\\3"))

    ;; Slack likes bold to use single asterisks:
    (goto-char (point-min))
    (while (re-search-forward bold-emphasis nil t)
      (replace-match "*\\1*"))

    ;; Finally, let's fix all links we might have:
    (goto-char (point-min))
    (while (re-search-forward md-link nil t)
      (replace-match "\\1 (\\2)"))

    (buffer-substring (point-min) (point-max))))

(defun ha/markdown-to-slack-buffer-block ()
  "Assume the current paragraph is an indented source code block.
This wraps it in the triple backticks needed for Slack."
  (insert "```")
  (while (re-search-forward "    " nil t)
    (replace-match "")
    (forward-line))
  (end-of-line)
  (insert "```"))

(defun ha/slack-to-markdown-buffer ()
  "Odd function that converts Slack’s version of Markdown (where
code is delimited with triple backticks) into a more formal
four-space indent markdown style."
  (goto-char (point-min))
  ;; Begin by converting all Carriage Returns to line feeds:
  (while (re-search-forward "" nil t)
    (replace-match "
"))

  (goto-char (point-min))
  (while (re-search-forward "```" nil t)
    (replace-match "

    ")
    (let ((starting-bounds (point)))
      (if (re-search-forward "```[ \t]*" nil t)
          (let ((ending-bounds (point)))
            (replace-match "

")
            (goto-char starting-bounds)
            (while (< (point) ending-bounds)
              (next-line)
              (beginning-of-line)
              (insert "    ")))))))

(defun ha/html-paste-touchup ()
  "Attempts to fix the org produced by `pandoc'' that seems to plague us."
  (interactive)
  (dolist (combo '((" (edited) " " ")   ; Slack appends this phrase that is never needed
                   (" " " ")             ; Pandoc's fixed space needs to go
                   ("\\\\\\\\$" "")     ; Pandoc's fixed space needs to go
                   ("\\[\\[https://slack-imgs\\.com/.*\\.png\\]\\]" "") ;; Emoticons associated with a user
                   ("\\[\\[https://.*\\.slack\\.com/archives.*\\]\\[\\(.*\n.*\\)\\]\\]" "")
                   ("\\[\\[https://app\.slack\.com/team.*\\]\\[\\(.*\\)\n\\(.*\\)\\]\\]" "  - *\\1 \\2:* ")
                   ("\\[\\[https://app\.slack\.com/team.*\\]\\[\\(.*\n.*\\)\\]\\]" "  - *\\1:* ")
                   ("^ *<<[0-9\.]+>>\n\n" ""))) ;; Slack includes these time things?
    (seq-let (search replace) combo
      (goto-char (point-min))
      (while (re-search-forward search nil t)
        (replace-match replace)))))

(defun ha/org-clipboard ()
  "Return the contents of the clipboard in org-mode format."
  (seq-let (type contents) (ha/get-clipboard)
    (with-temp-buffer
      (insert contents)
      (if (eq :html type)
          (shell-command-on-region (point-min) (point-max) "pandoc -f html -t org" t t)
        (ha/slack-to-markdown-buffer)
        (shell-command-on-region (point-min) (point-max) "pandoc -f markdown -t org" t t))
      (ha/html-paste-touchup)
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun ha/org-yank-clipboard ()
  "Yanks (pastes) the contents of the Apple Mac clipboard in an
org-mode-compatible format."
  (interactive)
  (insert (ha/org-clipboard)))

(defun ha/external-capture-to-org ()
  "Calls `org-capture-string' on the contents of the Apple clipboard."
  (interactive)
  (org-capture-string (ha/org-clipboard) "C")
  (ignore-errors
    (delete-frame)))

;; ------------------------------------------------------------
;;   Export org content to HTML-formatted clipboards
;; ------------------------------------------------------------

(defun ha/command-html-to-clipboard (html-file)
  "Return a command (suitable for `shell-command') to convert the
contents of HTML-FILE to the operating system's clipboard."
  (if (eq system-type 'darwin)
      (concat "hex=`hexdump -ve '1/1 \"%.2x\"' < "
              html-file
              "` \n"
              "osascript -e \"set the clipboard to «data HTML${hex}»\"")
    ;; TODO Add a version to convert HTML to Linux clipboard
    (concat "xclip -t text/html " html-file)))

(defun ha/org-html-with-header-to-clipboard ()
  "Converts region or subtree (with the section header) of the
current org file into HTML and copies the contents as HTML into
the operating system's clipboard."
  (interactive)
  (let ((html-file (org-html-export-to-html nil t t)))
    (shell-command (ha/command-html-to-clipboard html-file))))

(defun ha/org-html-to-clipboard ()
   "Converts region or subtree of the current org file into HTML
and copies the contents as HTML into the operating system's
clipboard."
   (interactive)
   (let ((html-file (org-html-export-to-html nil t t t)))
     (shell-command (ha/command-html-to-clipboard html-file))))

(defun ha/org-html-string-to-clipboard (st)
  "Converts an HTML-formatted string to an appropriate RTF clipboard"
  (interactive "sHTML Message:")
  (let ((html-file (make-temp-file "ha-" nil ".html" st)))
    (insert (ha/command-html-to-clipboard html-file))))

(defun ha/command-file-to-clipboard (md-file)
  "Return a command (suitable for `shell-command') to convert the
contents of MD-FILE to the operating system's clipboard."
  (if (eq system-type 'darwin)
      (concat "pbcopy < " md-file)
    (concat "xclip " md-file)))

(defun ha/org-to-md-clipboard ()
  "Converts region or subtree of the current org file into
Markdown and copies the contents into the operating system's
clipboard."
  (interactive)
  (let ((text-file (org-md-export-to-markdown nil t t)))
    (shell-command (ha/command-file-to-clipboard text-file))))

(defun ha/org-to-slack-clipboard ()
  "Converts region or subtree of the current org file into
Slack-format and copies the contents into the operating system's
clipboard."
  (interactive)
  (let ((buf-name "*Org MD Export*")
        (text-file (make-temp-file "ha-"))
        (org-export-show-temporary-export-buffer nil))
    (org-md-export-as-markdown nil t t)
    (with-current-buffer buf-name
      (ha/markdown-to-slack-buffer)
      (write-file text-file))
    (shell-command (ha/command-file-to-clipboard text-file))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; funcs.el ends here

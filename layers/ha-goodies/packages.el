;; -*- mode: emacs-lisp; fill-column: 75; -*-
;; vim: ft=lisp cc=75
;;; packages.el --- my-goodies layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: Howard Abrams <howard.abrams@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `my-goodies-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `my-goodies/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `my-goodies/pre-init-PACKAGE' and/or
;;   `my-goodies/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst ha-goodies-packages
  '(ag full-ack
    autoinsert
    counsel
    dired
    dired-narrow
    dired-subtree
    elfeed
    evil-collection
    fancy-narrow
    goto-addr
    ha-ligatures
    load-env-vars
    vterm
    rg
    synosaurus
    telega
    visual-regexp

    ;; Personal packages ...
    (demo-it :location local)
    (ha-ssh :location local)
    (piper :location local)))

(defun ha-goodies/init-demo-it ()
  "Installation from my local version of `demo-it'."
  (use-package demo-it
    :load-path "~/other/demo-it/"))

(defun ha-goodies/init-ha-ssh ()
  "Support for my personal SSH package."
  (use-package ha-ssh
    :defer 60
    :load-path "~/.spacemacs.d/elisp/"
    :config
    (spacemacs/declare-prefix "o s" "shells")
    (ha/set-leader-keys
     "o s o" '("overcloud" . ha/ssh-overcloud)
     "o s l" '("local shell" . ha/shell)
     "o s s" '("remote shell" . ha/ssh)
     "o s q" '("quit shell" . ha/ssh-exit)
     "o s f" '("find-file" . ha/ssh-find-file)
     "o s r" '("find-root" . ha/ssh-find-root))))

(defun ha-goodies/init-piper ()
  "Support for my personal replacement for a shell's pipe."
  (use-package piper
    :load-path "~/other/emacs-piper/"
    :config
    (spacemacs/declare-prefix "o |" "piper")
    (ha/set-leader-keys
     "|" '("piper-ui" . piper-user-interface)
     "o | |" '("piper-locally" . piper)
     "o | d" '("other-directory" . piper-other)
     "o | r" '("piper-remotely" . piper-remote))))

(defun ha-goodies/init-evil-collection ()
  "Not sure why evil-collection isn't installed with Spacemacs.
See https://github.com/emacs-evil/evil-collection ... also, I am
adding the modes individually as I see a need."
  (use-package evil-collection
    :after evil
    :ensure t
    :config
    (evil-collection-init '(eww dired notmuch term wdired))))

(defun ha-goodies/init-ag ()
  (use-package ag
    :ensure t
    :config
    (spacemacs/set-leader-keys
      "p s" 'projectile-ag)))

(defun ha-goodies/init-full-ack ()
  (use-package full-ack
    :ensure t
    :init
    (autoload 'ack-same "full-ack" nil t)
    (autoload 'ack "full-ack" nil t)
    (autoload 'ack-find-same-file "full-ack" nil t)
    (autoload 'ack-find-file "full-ack" nil t)

    :config
    ;; Having troubles with `projectile-ack', and perhaps this direct wrapper
    ;; will help with both using `full-ack' as well as address the annoying
    ;; mode-line issues associated with `ag':
    (spacemacs/set-leader-keys "p s" 'ack)
    (spacemacs/set-leader-keys "p /" 'projectile-ack)))

(defun ha-goodies/dictionary ()
  "I don't often need to look up word definitions, so I'm not
sure which I project I should use. Both the `define-word'project
(see https://github.com/abo-abo/define-word), and `helm-wordnet'
projects (see https://github.com/raghavgautam/helm-wordnet) have
good enough interfaces, but the grand-daddy of 'em all, the
`dictionary' project displays the definition, nicely rendered in
a side window.

See https://github.com/myrkr/dictionary-el for details.

Remember to call `dictionary-match-words' to help find something."
  (use-package dictionary
    :ensure t
    :config
    (spacemacs/set-leader-keys "S D" 'dictionary-search)))

(defun ha-goodies/init-synosaurus ()
  "Configuration for accessing a thesaurus (synonym finder).
Note: You must install wordnet for this work:  `apt install wordnet'
See https://github.com/hpdeifel/synosaurus for details."
  (use-package synosaurus
    :ensure t
    :init
    (use-package popup :ensure t)
    :config
    (setq synosaurus-backend 'synosaurus-backend-wordnet
          synosaurus-choose-method 'ido)
    (spacemacs/set-leader-keys "S T" 'synosaurus-choose-and-replace)))

(defun ha-goodies/post-init-counsel ()
  "Tweaks to Ivy's Counsel, mostly adding actions to standard stuff."
  (use-package counsel
    :after ivy
    :config
    (defun counsel-rg-dir (selected-file)
      "Search SELECTED-FILE's directory interactively using ripgrep."
      (let ((default-directory (f-parent selected-file)))
        (counsel-rg ivy-text nil "")))

    (ivy-add-actions
     'counsel-find-file
     '(("D" spacemacs/delete-file-confirm "delete file")
       ("R" spacemacs/rename-file "rename file")
       ("r" counsel-rg-dir "search")

       ;; Yes, selecting the "." with default action does the same thing now,
       ;; but I want it to be more explicit:
       ("d" dired "dired")))))

(defun ha-goodies/post-init-dired ()
  "Dired configuration from https://t.ly/39Ky5"
  (use-package dired
    :config
    (setq dired-isearch-filenames 'dwim
          delete-by-moving-to-trash t
          dired-dwim-target t
          dired-listing-switches "-AFlv")
    ;; With this ls switches, use - to go up a directory
    :hook
    (dired-mode . dired-hide-details-mode)))

(defun ha-goodies/post-init-find-dired ()
  "The `find-name-dired' is useful at times."
  (use-package find-dired
    :after dired
    :config
    (setq find-ls-option '("-ls" . "-AFlv")
          find-name-arg "-iname")
    (spacemacs/set-leader-keys "jF" 'find-name-dired)))

(defun ha-goodies/init-dired-narrow ()
  "Does what it says on the tin. Dynamically show only certain
files in a dired based on fixed words, regular expression, or my
favorite, fuzzy matching."
  (use-package dired-narrow
    :ensure t
    :after dired
    :bind (:map dired-mode-map
                ("%/" . dired-narrow)
                ("%%" . dired-narrow-regexp)
                ("%f" . dired-narrow-fuzzy))))

(defun ha-goodies/init-dired-subtree ()
  "Tree-style navigation across the filesystem.

 - The tab key will expand or contract the subdirectory at point.

 - S-TAB will behave just like org-mode handles its headings: hit
   it once to expand a subdir at point, twice to do it
   recursively, thrice to contract the tree."
  (use-package dired-subtree
    :ensure t
    :after dired
    :bind (:map dired-mode-map
                ("<tab>" . dired-subtree-toggle)
                ("<S-tab>" . dired-subtree-cycle))))

(defun ha-goodies/init-rg ()
  "Installation/configuration for https://github.com/dajva/rg.el"
  (use-package rg
    :ensure t
    :config
    (rg-enable-default-bindings (kbd "M-s"))
    (spacemacs/declare-prefix "o /" "search")
    (spacemacs/set-leader-keys    ; Shame that the rename feature doesn't work
     "p s" '("projectile-search" . rg-project)
     "o / s" '("search-symbol"   . rg-dwim)
     "o / h" '("search-history"  . rg-history)
     "o / g" '("search-ripgrep"  . rg))))

(defun ha-goodies/init-telega ()
  "See https://github.com/zevlg/telega.el for details.

To install: brew install tdlib ... And if `tdlib' gets upgrade,
jump into the ELPA repository for it, and `make clean' and `make
install' to link to the upgraded library."
  (use-package telega
    :ensure t
    :commands (telega)
    :defer t
    :config
    (telega-mode-line-mode 1)
    (telega-notifications-mode 1)
    (add-hook 'telega-chat-mode-hook
              (lambda ()
                (set (make-local-variable 'company-backends)
                     (append '(telega-company-emoji
                               telega-company-username
                               telega-company-hashtag)
                             (when (telega-chat-bot-p telega-chatbuf--chat)
                               '(telega-company-botcmd))))
                (company-mode 1)))))

(defun ha-goodies/init-visual-regexp ()
  (use-package visual-regexp
    :ensure t
    :bind (("C-c r" . vr/replace)
           ("C-c q" . vr/query-replace))
    :config
    (spacemacs/set-leader-keys
      "r r" 'vr/replace
      "r q" 'vr/query-replace
      "r m" 'vr/mc-mark)))

(defun ha-goodies/init-autoinsert ()
  (use-package autoinsert
    :init
    (setq
     auto-insert 'other
     auto-insert-directory (concat (getenv "HOME")
                                   "/.spacemacs.d/templates/")
     ;; Don't want to be prompted before insertion:
     auto-insert-query nil)
    (add-hook 'find-file-hook 'auto-insert)
    (auto-insert-mode 1)

    :config
    ;; Every template that begins with `default' will be automatically added as
    ;; the default template for a particular file extension.
    (dolist (template (directory-files auto-insert-directory nil "default.*"))
      (let* ((ext (file-name-extension template))
             (pattern (concat "\\." ext "$")))
        (define-auto-insert pattern (vector template 'ha/autoinsert-yas-expand))))

    (define-auto-insert "Sprint.*\\.\\org\\'" ["sprint.org" ha/autoinsert-yas-expand])
    (define-auto-insert "test_.*\\.\\rb\\'" ["test_spec.rb" ha/autoinsert-yas-expand])
    (define-auto-insert "/bin/"  ["default.sh" ha/autoinsert-yas-expand])))

(defun ha-goodies/post-init-git-timemachine ()
  "Configure git-timemachine: https://github.com/pidu/git-timemachine"
  (spacemacs/set-leader-keys
    "g f t" 'git-timemachine)

  (defun git-timemachine-show-commit ()
    (interactive)
    (magit-show-commit (car git-timemachine-revision))))

(defun ha-goodies/post-init-elfeed ()
  "Personal extensions to the Elfeed."
  (add-hook 'elfeed-show-mode-hook #'visual-line-mode)

  (with-eval-after-load "elfeed-search"
    (define-key elfeed-search-mode-map (kbd "q") 'ha/elfeed-bury))
  (with-eval-after-load "elfeed-show"
    (define-key elfeed-show-mode-map (kbd "L") 'elfeed-goodies/split-show-next))
  (with-eval-after-load "elfeed-show"
    (define-key elfeed-show-mode-map (kbd "H") 'elfeed-goodies/split-show-previous))

  ;; Found on http://pragmaticemacs.com/category/elfeed/
  (with-eval-after-load "elfeed-search"
    (defalias 'elfeed-toggle-star
      (elfeed-expose #'elfeed-search-toggle-all 'star))
    (define-key elfeed-search-mode-map (kbd "m") 'elfeed-toggle-star))

  (spacemacs/set-leader-keys "a f" 'ha/elfeed)
  (spacemacs|define-custom-layout "@elfeed"
    :binding "F"
    :body (call-interactively 'ha/elfeed))

  ;; Many of these are repeats of the existing non-comma keysquences, or those
  ;; that might have been overshadowed by the normal state:
  (ha/set-leader-keys-for-major-mode 'elfeed-show-mode
                                     ","  '("scroll"       . scroll-up-command)
                                     "b"  '("visit-web"    . elfeed-show-visit)
                                     "q"  '("quit"  . ha/elfeed-bury)
                                     "g"  '("refresh"      . elfeed-show-refresh)
                                     "n"  '("next-entry"   . elfeed-goodies/split-show-next)
                                     "p"  '("previous-url" . elfeed-goodies/split-show-prev)))

(defun ha-goodies/init-fancy-narrow ()
  "Configure the fancy-narrow package: https://github.com/Bruce-Connor/fancy-narrow

This project works really well for code-reviews and
presentations, and I just make it a little easier to work with by
wrapping the standard functions. "

  (use-package fancy-narrow
    :ensure t
    :config
    (defun ha/highlight-block ()
      "Highlights a 'block' in a buffer defined by the first
      blank line before and after the current cursor position.
      Uses the 'fancy-narrow' mode to high-light the block."
      (interactive)
      (let (cur beg end)
        (setq cur (point))
        (setq end (or (re-search-forward  "^\s*$" nil t) (point-max)))
        (goto-char cur)
        (setq beg (or (re-search-backward "^\s*$" nil t) (point-min)))
        (fancy-narrow-to-region beg end)
        (goto-char cur)))

    (defvar ha/highlight-symbol-inv-colors
      '(hi-red hi-blue hi-green hi-pink hi-yellow)
      "A list of favorite colors")
    (defvar ha/highlight-symbol-colors
      '(hi-red-b hi-blue-b hi-green-b)
      "A list of favorite colors")

    (defun ha/bold-symbol (bolder)
      "Bolds the current thing we are pointing."
      (interactive "p")
      (let ((regexp (find-tag-default-as-symbol-regexp)))
        (if (= bolder 4)
            (highlight-regexp regexp 'hi-black-hb)
          (highlight-regexp regexp 'hi-black-b))))

    (defun ha/big-bold-symbol ()
      "Really bolds the current thing we are pointing."
      (interactive)
      (let ((regexp (find-tag-default-as-symbol-regexp)))
        (highlight-regexp regexp 'hi-black-hb)))

    (defun ha/highlight-symbol (choice)
      "Highlights the symbol at point. The color is automatically
chosen, and rotated through a list."
      (interactive "p")
      ;; Rotate our color list, so that we can choose the first.
      (if (= choice 4)
          (setq ha/highlight-symbol-inv-colors
                (-rotate 1 ha/highlight-symbol-inv-colors))
        (setq ha/highlight-symbol-colors
              (-rotate 1 ha/highlight-symbol-colors)))

      (let ((regexp  (find-tag-default-as-symbol-regexp))
            (hicolor (car (if (= choice 4)
                              ha/highlight-symbol-inv-colors
                            ha/highlight-symbol-colors))))
        (highlight-regexp regexp hicolor)))

    (defun ha/highlight-dwim (num)
      "If some of the buffer is highlighted with the `fancy-narrow'
      mode, then un-highlight it by calling `fancy-widen'.

      If region is active, call `fancy-narrow-to-region'.

      If NUM is 0, highlight the current block (delimited by
      blank lines). If NUM is positive or negative, highlight
      that number of lines. Otherwise, called
      `fancy-narrow-to-defun', to highlight current function."
         (interactive "p")
         (unless (eq major-mode 'org-mode)
           (cond
            ((fancy-narrow-active-p)  (fancy-widen))
            ((region-active-p)        (progn
                                        (fancy-narrow-to-region (region-beginning) (region-end))
                                        (deactivate-mark)))
            ((= num 0)                (ha/highlight-block))
            ((= num 1)                (fancy-narrow-to-defun))
            (t                        (progn (ha/expand-region num)
                                             (fancy-narrow-to-region (region-beginning) (region-end))
                                             (setq mark-active nil))))))

    (defun ha/unhighlight-all ()
      (interactive)
      (unhighlight-regexp t))

    ;; We really need to figure out a leader situation...
    (spacemacs/declare-prefix "o h" "highlight")
    (ha/set-leader-keys
      "o h h" '("dwim" . ha/highlight-dwim)
      "o h u" '("unhilite-symbol" . ha/unhighlight-all)
      "o h o" '("block" . ha/highlight-block)
      "o h f" '("function" . fancy-narrow-to-defun)
      "o h r" '("region" . fancy-narrow-to-region)
      "o h s" '("hilite-symbol" . ha/highlight-symbol)
      "o h b" '("bold-symbol" . ha/bold-symbol)
      "o h B" '("bolder-symbol" . ha/big-bold-symbol))

    :bind
    ("S-<f16>" . ha/highlight-dwim)
    ("<f13>"   . ha/highlight-dwim)))

(defun ha-goodies/init-goto-addr ()
  "Trying out the `goto-addr' project."
  (use-package goto-addr
   :hook ((compilation-mode . goto-address-mode)
          (prog-mode . goto-address-prog-mode)
          (eshell-mode . goto-address-mode)
          (shell-mode . goto-address-mode))
   :bind (:map goto-address-highlight-keymap
               ("<RET>" . goto-address-at-point)
               ("M-<RET>" . newline))
   :commands (goto-address-prog-mode
              goto-address-mode)))

(defun ha-goodies/init-ha-ligatures ()
  "Configures my default font as well as ligatures using prettify-symbols."
  (use-package ha-ligatures
    :load-path "~/.spacemacs.d/elisp/"))

(defun ha-goodies/init-load-env-vars ()
  "Configure a package that allows you to source in environment variables.
See: https://github.com/diasjorge/emacs-load-env-vars/"
  (use-package load-env-vars
    :ensure t))

(defun ha-goodies/init-md4rd ()
  "Configuration of Reddit app, see https://github.com/ahungry/md4rd.
I find the keybindings and general UI a bit confusing to remember,
but it seems to work well enough."
  (use-package md4rd
    :defer 10
    :init
    (setq md4rd-subs-active '(emacs orgmode Clojure DungeonsAndDragons))
    :config
    (setq md4rd--oauth-access-token
          (ha/get-private-data 'reddit-oauth-access-token)
          md4rd--oauth-refresh-token
          (ha/get-private-data 'reddit-oauth-refresh-token))
    (ha/set-leader-keys
     "o r" '("reddit" . md4rd))

    (spacemacs|define-custom-layout "@reddit"
      :binding "R"
      :body (call-interactively 'md4rd))))

(defun ha-goodies/post-init-vterm ()
  "docstring"
  (use-package vterm
    :ensure t
    :config
    (add-hook 'vterm-mode-hook
              (lambda ()
                (setq-local evil-insert-state-cursor 'box)
                (evil-insert-state)))
    (define-key vterm-mode-map [return]                      #'vterm-send-return)

    (setq vterm-keymap-exceptions nil)
    (evil-define-key 'insert vterm-mode-map (kbd "C-e")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-f")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-a")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-v")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-b")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-w")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-u")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-d")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-n")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-m")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-p")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-j")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-k")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-r")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-t")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-g")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-c")      #'vterm--self-insert)
    (evil-define-key 'insert vterm-mode-map (kbd "C-SPC")    #'counsel-M-x)
    (evil-define-key 'normal vterm-mode-map (kbd "C-d")      #'vterm--self-insert)
    (evil-define-key 'normal vterm-mode-map (kbd ",c")       #'multi-vterm)
    (evil-define-key 'normal vterm-mode-map (kbd ",n")       #'multi-vterm-next)
    (evil-define-key 'normal vterm-mode-map (kbd ",p")       #'multi-vterm-prev)
    (evil-define-key 'normal vterm-mode-map (kbd ",q")       #'ha/ssh-exit)
    (evil-define-key 'normal vterm-mode-map (kbd "i")        #'evil-insert-resume)
    (evil-define-key 'normal vterm-mode-map (kbd "o")        #'evil-insert-resume)
    (evil-define-key 'normal vterm-mode-map (kbd "<return>") #'evil-insert-resume)))

;;; packages.el ends here

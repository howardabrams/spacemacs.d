;;; CONFIG --- Summary
;;
;; Author: Howard X. Abrams <howard.abrams@workday.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created: 13 February 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(setq eshell-prompt-function
      (function
       (lambda ()
         (epe-colorize-with-face
          (if (= (user-uid) 0) "# " "$ ")
          'hi-blue-b))))

(when (spacemacs/system-is-mac)
  (setq insert-directory-program (or (executable-find "gls") "ls")
        dired-listing-switches "-aBhl --group-directories-first")

  ;; Disable fuzzy matching to make mdfind work with helm-locate
  ;; https://github.com/emacs-helm/helm/issues/799
  (setq helm-locate-fuzzy-match nil)
  (setq helm-locate-command "mdfind -name %s %s"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; config.el ends here

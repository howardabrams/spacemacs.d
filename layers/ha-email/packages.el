;; -*- mode: emacs-lisp; fill-column: 75; -*-
;;
;;; packages.el --- ha-email layer packages file for Spacemacs.
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;;
;;; Commentary:
;;
;; Using MU to read my personal mail at a few of my email address inboxes.
;;
;;; Code:

(defun find-installation-path (source-code-name)
  "Return the parent directory of the source code located somewhere in /usr."
  (let* ((pattern "find /usr/share usr/local/share /usr/local/Cellar -name %s.el 2>/dev/null | tail -1")
         (command (format pattern source-code-name))
         (results   (shell-command-to-string command))
         (last-line (string-match (rx (group (one-or-more any)) "\n" eol) results))
         (source-file (match-string 1 results)))
    (message "Looking for `%s' ... %s" source-code-name results)
    (when source-file
      (file-name-directory source-file))))

;; MU doesn't come with emacs, and needs to be installed as a package. While the
;; executable is available, the Emacs packages for MU is installed in different
;; places depending on the operating system. Doesn't take much to call `find' to
;; locate this and add it to `mu4e-installation-path' ... ugh.

(when (not (require 'mu4e nil t))
  (setq mu4e-installation-path (find-installation-path "mu4e")))

(setq ha-email-packages
      '((mu4e :variables
              mu4e-enable-async-operations t
              mu4e-enable-mode-line t         ;; we'll see...
              mu4e-enable-notifications t
              mu4e-use-maildirs-extension t)
        notmuch))


(defun ha-email/pre-init-mu4e ()
  "Overriding some MU4E Settings done before the package is loaded."
  (setq mu4e-alert-set-default-style 'notifier
        mu4e-compose-signature "(Why yes, this message was sent from Emacs)"
        mu4e-compose-signature-auto-include nil
        mu4e-context-policy 'pick-first
        mu4e-drafts-folder "/drafts"
        mu4e-get-mail-command "mbsync -a"
        mu4e-maildir "~/.mail"
        mu4e-update-interval 900
        mu4e-use-fancy-chars t
        mu4e-view-show-addresses t
        mu4e-view-show-images t
        message-kill-buffer-on-exit t

        mu4e-maildir-shortcuts
        '(("/gmail/INBOX" . ?g)
          ("/personal/INBOX" . ?p))

        mu4e-bookmarks
        `((mu4e-bookmark-define "date:today..now AND NOT flag:trashed"
                                "Today's messages" ?t)
          (mu4e-bookmark-define "date:7d..now AND NOT flag:trashed"
                                "Last 7 days" ?w)
          (mu4e-bookmark-define "date:7d..now AND flag:unread AND NOT flag:trashed"
                                "Recent Unread messages" ?u)
          (,(mapconcat 'identity
                       (mapcar
                        (lambda (maildir)
                          (concat "maildir:" (car maildir)))
                        mu4e-maildir-shortcuts) " OR ")
           "All inboxes" ?i))))

(defun ha-email/post-init-mu4e ()
  "Overrides default MU4E bookmarks with my versions."

  (use-package mu4e-context
    :load-path mu4e-installation-path
    :config
    (setq mu4e-contexts
          `( ,(make-mu4e-context
               :name "Personal"
               :enter-func (lambda () (mu4e-message "Switch to the Personal context"))
               :match-func (lambda (msg)
                             (when msg
                               (mu4e-message-contact-field-matches msg '(:to :cc) "howard@howardabrams.com")))
               :vars '((user-mail-address . "howard@howardabrams.com")
                       (mu4e-trash-folder . "/gmail/[Gmail].Trash")
                       (mu4e-sent-folder . "/gmail/[Gmail].Sent Mail")
                       (mu4e-refile-folder . "/gmail/[Gmail].Archive")
                       (smtpmail-starttls-credentials '(("smtp.googlemail.com" 587 nil nil)))))

             ,(make-mu4e-context
               :name "GooMail"
               :enter-func (lambda () (mu4e-message "Switch to the Other context"))
               :match-func (lambda (msg)
                             (when msg
                               (mu4e-message-contact-field-matches msg '(:to :cc) "howard.abrams@gmail.com")))
               :vars '((user-mail-address . "howard.abrams@gmail.com")
                       (mu4e-trash-folder . "/personal/[Gmail].Trash")
                       (mu4e-sent-folder . "/personal/[Gmail].Sent Mail")
                       (mu4e-refile-folder . "/personal/[Gmail].Archive")
                       (smtpmail-starttls-credentials '(("smtp.googlemail.com" 587 nil nil)))))))))


(defun ha-email/post-init-notmuch ()
  "Most of these setting were taken from "
  (use-package notmuch
    :ensure t
    :init
    (setq notmuch-mail-dir (format "%s/%s" (getenv "HOME") ".mail")
          notmuch-hooks-dir (expand-file-name ".notmuch/hooks" notmuch-mail-dir))

    :config
    (use-package ol-notmuch)

    (spacemacs|define-custom-layout "@mail"
      :binding "M"
      :body (call-interactively 'notmuch))

    (spacemacs/set-leader-keys
      "a e n u" 'notmuch-retrieve-messages)

    (define-key notmuch-hello-mode-map (kbd "U") 'notmuch-retrieve-messages)
    (define-key notmuch-hello-mode-map (kbd "C") 'notmuch-mua-new-mail)

    (use-package ol-notmuch)))

;; We may want to include notmuch-labeler and counsel-notmuch
;;; packages.el ends here

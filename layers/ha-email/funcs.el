;;; FUNCS --- Helper functions for my Mail Configuration
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created: 10 July 2020
;;
;;; Code:

(defun notmuch-retrieve-messages ()
  "Retrieve and process my mail messages."
  (interactive)
  (async-shell-command "notmuch new"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; funcs.el ends here

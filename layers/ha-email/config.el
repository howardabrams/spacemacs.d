;;; CONFIG --- Notmuch configuration and other settings
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created: 10 July 2020
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;   Originally taken from Vedang Manerikar's video:
;;      https://youtu.be/wuSPssykPtE
;;   And their code:
;;      https://gist.github.com/vedang/26a94c459c46e45bc3a9ec935457c80f
;;
;;   The ideas brought out were to mimic the hey.com email workflow,
;;   and while not bad, I thought that maybe I could improve upon it
;;   slowly over time.
;;
;;   To allow me to keep Vedang's and my code side-by-side in the same Emacs
;;   variable state, I have renamed the prefix to `ha-', however, if you are
;;   looking to steal my code, you may want to revisit the source. 😊
;;
;;; Code:

(setq notmuch-saved-searches '((:name "Imbox"
                                      :query "tag:inbox AND tag:screened AND tag:unread"
                                      :key "i"
                                      :search-type 'tree)
                               (:name "Previously Seen"
                                      :query "tag:screened AND NOT tag:unread"
                                      :key "I")
                               (:name "Unscreened"
                                      :query "tag:inbox AND tag:unread AND NOT tag:screened AND NOT date:..14d AND NOT tag:thefeed AND NOT tag:/ledger/ AND NOT tag:old-project"
                                      :key "s")
                               (:name "New Feed"
                                      :query "tag:thefeed AND tag:unread"
                                      :key "f"
                                      :search-type 'tree)
                               (:name "Old Feed"
                                      :query "tag:thefeed"
                                      :key "f"
                                      :search-type 'tree)
                               (:name "New Receipts"
                                      :query "tag:/ledger/ AND tag:unread"
                                      :key "p")
                               (:name "Papertrail"
                                      :query "tag:/ledger/"
                                      :key "p")

                               ;; (push '(:name "Projects"
                               ;;               :query "tag:project AND NOT tag:unread"
                               ;;               :key "x")
                               ;;       notmuch-saved-searches)
                               (:name "Old Projects"
                                      :query "tag:old-project AND NOT tag:unread"
                                      :key "X")))

;; Sign messages by default.
;; (add-hook 'message-setup-hook 'mml-secure-sign-pgpmime)

(setq notmuch-address-selection-function
      (lambda (prompt collection initial-input)
        (completing-read prompt
                         (cons initial-input collection)
                         nil
                         t
                         nil
                         'notmuch-address-history)))

;; The HEY Workflow Bindings

;; Bindings in `notmuch-show-mode'
(eval-after-load 'notmuch-show
  '(progn
     ;; Integrate with org-mode
     (require 'ol-notmuch)

     (define-key notmuch-show-mode-map (kbd "C") 'hey/notmuch-reply-later)))

;; Bindings in `notmuch-search-mode'
(eval-after-load 'notmuch-search
  '(progn
     (define-key notmuch-search-mode-map (kbd "r") 'notmuch-search-reply-to-thread)
     (define-key notmuch-search-mode-map (kbd "R") 'notmuch-search-reply-to-thread-sender)
     (define-key notmuch-search-mode-map (kbd "/") 'notmuch-search-filter)
     (define-key notmuch-search-mode-map (kbd "A") 'hey/notmuch-archive-all)
     (define-key notmuch-search-mode-map (kbd "D") 'hey/notmuch-delete-all)
     (define-key notmuch-search-mode-map (kbd "L") 'hey/notmuch-filter-by-from)
     (define-key notmuch-search-mode-map (kbd ";") 'hey/notmuch-search-by-from)
     (define-key notmuch-search-mode-map (kbd "d") 'hey/notmuch-search-delete-and-archive-thread)

     (define-key notmuch-search-mode-map (kbd "S") 'hey/notmuch-move-sender-to-spam)
     (define-key notmuch-search-mode-map (kbd "I") 'hey/notmuch-move-sender-to-screened)
     (define-key notmuch-search-mode-map (kbd "P") 'hey/notmuch-move-sender-to-papertrail)
     (define-key notmuch-search-mode-map (kbd "f") 'hey/notmuch-move-sender-to-thefeed)
     (define-key notmuch-search-mode-map (kbd "C") 'hey/notmuch-reply-later)))

;; Bindings in `notmuch-tree-mode'
(eval-after-load 'notmuch-tree
  '(progn
     (define-key notmuch-tree-mode-map (kbd "C") 'hey/notmuch-reply-later)))

(defun hey/notmuch-archive-all ()
  "Archive all the emails in the current view."
  (interactive)
  (notmuch-search-archive-thread nil (point-min) (point-max)))

(defun hey/notmuch-delete-all ()
  "Archive all the emails in the current view.
Mark them for deletion by cron job."
  (interactive)
  (notmuch-search-tag-all '("+deleted"))
  (hey/notmuch-archive-all))

(defun hey/notmuch-search-delete-and-archive-thread ()
  "Archive the currently selected thread. Add the deleted tag as well."
  (interactive)
  (notmuch-search-add-tag '("+deleted"))
  (notmuch-search-archive-thread))

(defun hey/notmuch-tag-and-archive (tag-changes &optional beg end)
  "Prompt the user for TAG-CHANGES.
Apply the TAG-CHANGES to region and also archive all the emails.
When called directly, BEG and END provide the region."
  (interactive (notmuch-search-interactive-tag-changes))
  (notmuch-search-tag tag-changes beg end)
  (notmuch-search-archive-thread nil beg end))

(defun hey/notmuch-search-find-from ()
  "A helper function to find the email address for the given email."
  (let ((notmuch-addr-sexp (first
                            (notmuch-call-notmuch-sexp "address"
                                                       "--format=sexp"
                                                       "--format-version=1"
                                                       "--output=sender"
                                                       (notmuch-search-find-thread-id)))))
    (plist-get notmuch-addr-sexp :address)))

(defun hey/notmuch-filter-by-from ()
  "Filter the current search view to show all emails sent from the sender of the current thread."
  (interactive)
  (notmuch-search-filter (concat "from:" (hey/notmuch-search-find-from))))

(defun hey/notmuch-search-by-from (&optional no-display)
  "Show all emails sent from the sender of the current thread.
NO-DISPLAY is sent forward to `notmuch-search'."
  (interactive)
  (notmuch-search (concat "from:" (hey/notmuch-search-find-from))
                  notmuch-search-oldest-first
                  nil
                  nil
                  no-display))

(defun hey/notmuch-tag-by-from (tag-changes &optional beg end refresh)
  "Apply TAG-CHANGES to all emails from the sender of the current thread.
BEG and END provide the region, but are ignored. They are defined
since `notmuch-search-interactive-tag-changes' returns them. If
REFRESH is true, refresh the buffer from which we started the
search."
  (interactive (notmuch-search-interactive-tag-changes))
  (let ((this-buf (current-buffer)))
    (hey/notmuch-search-by-from t)
    ;; This is a dirty hack since I can't find a way to run a
    ;; temporary hook on `notmuch-search' completion. So instead of
    ;; waiting on the search to complete in the background and then
    ;; making tag-changes on it, I will just sleep for a short amount
    ;; of time. This is generally good enough and works, but is not
    ;; guaranteed to work every time. I'm fine with this.
    (sleep-for 0.5)
    (notmuch-search-tag-all tag-changes)
    (when refresh
      (set-buffer this-buf)
      (notmuch-refresh-this-buffer))))

(defun hey/notmuch-add-addr-to-db (nmaddr nmdbfile)
  "Add the email address NMADDR to the db-file NMDBFILE."
  (append-to-file (format "%s\n" nmaddr) nil nmdbfile))

(defun hey/notmuch-move-sender-to-thefeed ()
  "For the email at point, move the sender of that email to the feed.
This means:
1. All new email should go to the feed and skip the inbox altogether.
2. All existing email should be updated with the tag =thefeed=.
3. All existing email should be removed from the inbox."
  (interactive)
  (hey/notmuch-add-addr-to-db (hey/notmuch-search-find-from)
                                 (format "%s/thefeed.db" notmuch-hooks-dir))
  (hey/notmuch-tag-by-from '("+thefeed" "+archived" "-inbox")))

(defun hey/notmuch-move-sender-to-papertrail (tag-name)
  "For the email at point, move the sender of that email to the papertrail.
This means:
1. All new email should go to the papertrail and skip the inbox altogether.
2. All existing email should be updated with the tag =ledger/TAG-NAME=.
3. All existing email should be removed from the inbox."
  (interactive "sTag Name: ")
  (hey/notmuch-add-addr-to-db (format "%s %s"
                                         tag-name
                                         (hey/notmuch-search-find-from))
                                 (format "%s/ledger.db" notmuch-hooks-dir))
  (let ((tag-string (format "+ledger/%s" tag-name)))
    (hey/notmuch-tag-by-from (list tag-string "+archived" "-inbox" "-unread"))))

(defun hey/notmuch-move-sender-to-screened ()
  "For the email at point, move the sender of that email to Screened Emails.
This means:
1. All new email should be tagged =screened= and show up in the inbox.
2. All existing email should be updated to add the tag =screened=."
  (interactive)
  (hey/notmuch-add-addr-to-db (hey/notmuch-search-find-from)
                                 (format "%s/screened.db" notmuch-hooks-dir))
  (hey/notmuch-tag-by-from '("+screened")))

(defun hey/notmuch-move-sender-to-spam ()
  "For the email at point, move the sender of that email to spam.
This means:
1. All new email should go to =spam= and skip the inbox altogether.
2. All existing email should be updated with the tag =spam=.
3. All existing email should be removed from the inbox."
  (interactive)
  (hey/notmuch-add-addr-to-db (hey/notmuch-search-find-from)
                                 (format "%s/spam.db" notmuch-hooks-dir))
  (hey/notmuch-tag-by-from '("+spam" "+deleted" "+archived" "-inbox" "-unread" "-screened")))

(defun hey/notmuch-reply-later ()
  "Capture this email for replying later."
  (interactive)
  ;; You need `org-capture' to be set up for this to work. Add this
  ;; code somewhere in your init file after `org-cature' is loaded:

  ;; (push '("r" "Respond to email"
  ;;         entry (file org-default-notes-file)
  ;;         "* TODO Respond to %:from on %:subject  :email: \nSCHEDULED: %t\n%U\n%a\n"
  ;;         :clock-in t
  ;;         :clock-resume t
  ;;         :immediate-finish t)
  ;;       org-capture-templates)

  (org-capture nil "r")

  ;; The rest of this function is just a nice message in the modeline.
  (let* ((email-subject (format "%s..."
                                (substring (notmuch-show-get-subject) 0 15)))
         (email-from (format "%s..."
                             (substring (notmuch-show-get-from) 0 15)))
         (email-string (format "%s (From: %s)" email-subject email-from)))
    (message "Noted! Reply Later: %s" email-string)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; config.el ends here

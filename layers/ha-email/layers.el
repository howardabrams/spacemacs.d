;;; LAYERS --- Pull in the Spacemacs MU4E and the Notmuch layers
;;
;; Author: Howard X. Abrams <howard@howardabrams.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created:  7 May 2020
;;
;;; Code:

(configuration-layer/declare-layer '(mu4e :variables
                                          mail-user-agent 'mu4e-user-agent
                                          mu4e-use-maildirs-extension t
                                          org-mu4e-link-query-in-headers-mode nil
                                          mu4e-spacemacs-layout-name "@Mu4e"
                                          mu4e-spacemacs-layout-binding "M"))

(configuration-layer/declare-layer '(notmuch :variables
                                             notmuch-message-deleted-tags '("+deleted" "-inbox" "-unread")
                                             message-kill-buffer-on-exit t
                                             message-send-mail-function 'message-send-mail-with-sendmail
                                             message-sendmail-envelope-from 'header
                                             mail-specify-envelope-from t
                                             notmuch-archive-tags '("-inbox" "-unread" "+archived")
                                             notmuch-show-mark-read-tags '("-inbox" "-unread" "+archived")
                                             notmuch-search-oldest-first nil
                                             notmuch-show-indent-content nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; layers.el ends here
